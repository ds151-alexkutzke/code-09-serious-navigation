import React from "react";
import { View, Text, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { SearchBar } from "react-native-elements";

const Drawer = createDrawerNavigator();
const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

const HomeStackScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Test" component={TestScreen} />
    </Stack.Navigator>
  );
};

const FeedScreen = () => {
  const insets = useSafeAreaInsets();

  return (
    <>
      <View style={{paddingTop: insets.top}}>
        <SearchBar placeholder="Type Here..." lightTheme={true} />
      </View>
      <Tab.Navigator>
        <Tab.Screen name="HomeStack" component={HomeStackScreen} />
        <Tab.Screen name="Popular" component={PopularScreen} />
        <Tab.Screen name="Awarded" component={AwardedScreen} />
      </Tab.Navigator>
    </>
  );
};

function TestScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Test</Text>
    </View>
  );
}

function PopularScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Popular</Text>
    </View>
  );
}

function AwardedScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Awarded</Text>
    </View>
  );
}

function SettingsScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Settings</Text>
    </View>
  );
}

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Home Screen</Text>
      <Button
        title="Vai para Test"
        onPress={() => navigation.navigate("Test")}
      />
    </View>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Feed">
        <Drawer.Screen name="Feed" component={FeedScreen} />
        <Drawer.Screen name="Settings" component={SettingsScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default App;
